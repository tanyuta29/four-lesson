public abstract sealed class Part permits Cube, Sphere, Tetrahedron {

    private double density; // густина, в грамах на 1 мм³

    public Part(double density) {
        this.density = density;
    }

    public double getDensity() {
        return density;
    }

    public void setDensity(double density) {
        this.density = density;
    }
static final double LAYER_THICKNESS_CONSTANT = 30;
    public double getProtectiveLayerThickness(double weightInGrams) {
        double weightInKilograms = weightInGrams / 1000;
        return weightInKilograms / LAYER_THICKNESS_CONSTANT ; // Підказка: це є магічною цифрою
    }}
